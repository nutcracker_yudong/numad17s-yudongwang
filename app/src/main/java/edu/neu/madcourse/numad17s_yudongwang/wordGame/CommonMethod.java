package edu.neu.madcourse.numad17s_yudongwang.wordGame;

import android.content.Context;
import android.media.MediaPlayer;

import java.util.List;

/**
 * Created by yudong on 2/17/17.
 */

public class CommonMethod {
    public static MediaPlayer player;
    public static boolean music;
    public static int previousMusic;
    public static Context preContext;

    public static long time;
    public static Integer score;
    public static int stage1Score;
    public static int stage2Score;

    public static boolean loadMode;
    public static String[] input;
    public static boolean enterStageTwo;

    public static boolean currentGame;
    public static int resetTimes;

    public static String timePlayed;
    public static String longestWord;
    public static Integer wordHighestScore;
    public static String userName;

    public static String token;

    public static boolean isPlaying;
    public static String message;


    public static void SoundPlayer(Context ctx, int raw_id){
        previousMusic = raw_id;
        preContext = ctx;
        getMusicState();

        if (music) {
            if (player!= null) player.release();
                player = MediaPlayer.create(ctx, raw_id);
//            player.setLooping(true); // Set looping
                player.setVolume(100, 100);
                player.start();
//            }
        }
    }

    public static void SoundMute() {
        music = false;
        getMusicState();
    }

    public static void SoundStop() {

        if(player!= null){
            player.stop();
        }
    }

    public static void SoundOn() {
        music = true;
        getMusicState();
    }

    public static void SoundResume() {
        player.start();
    }

    public static void SoundMyPause() {
        player.pause();
    }

    public static void getMusicState() {
    }

}
