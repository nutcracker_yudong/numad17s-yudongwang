package edu.neu.madcourse.numad17s_yudongwang.wordGame;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

import edu.neu.madcourse.numad17s_yudongwang.R;
import edu.neu.madcourse.numad17s_yudongwang.firebase.fcm.FCMActivity;
import edu.neu.madcourse.numad17s_yudongwang.firebase.realTime.models.User;


public class CustomAdapter extends ArrayAdapter<CustomAdapter.Record> {

    private List<Record> items;
    private int layoutResourceId;
    private Context context;

    public CustomAdapter(Context context, int layoutResourceId, List<Record> items) {
        super(context, layoutResourceId, items);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        View row = convertView;
        OneRecordHolder holder = null;

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);

        holder = new OneRecordHolder();
        holder.record = items.get(position);

        holder.sendButton = (Button) row.findViewById(R.id.btn_leaderBoard_send);
//        holder.sendButton.setTag(holder.record);

        final OneRecordHolder finalHolder = holder;
        final OneRecordHolder finalHolder1 = holder;
        holder.sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                pushNotificationToPerson(finalHolder1.record.user.token);

                Toast.makeText(getContext(), "You sent a congratulations to " + finalHolder.record.user.username, Toast.LENGTH_SHORT).show();
            }
        });


        holder.detail = (TextView) row.findViewById(R.id.text_leaderBoard_details);

        row.setTag(holder);

        setupItem(holder);
        return row;
    }

    private void setupItem(OneRecordHolder holder) {
        holder.detail.setText(holder.record.getDetail());
    }

    public void sendCong(View v) {
        Record itemToRemove = (Record) v.getTag();
    }


    public static class OneRecordHolder {
        Record record;
        TextView detail;
        Button sendButton;

        @Override
        public String toString() {
            return "OneRecordHolder{" +
                    "record=" + record +
                    ", detail=" + detail +
                    ", sendButton=" + sendButton +
                    '}';
        }
    }

    static class Record implements Serializable {
        private String detail;
        private User user;

        public Record(String detail, User user) {
            this.detail = detail;
            this.user = user;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        @Override
        public String toString() {
            return "Record{" +
                    "detail='" + detail + '\'' +
                    ", user=" + user +
                    '}';
        }
    }



    public void pushNotificationToPerson(final String toToken) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                anotherThreadNotification(toToken);
            }
        }).start();
    }

    public void anotherThreadNotification(final String toToken) {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        try {
            jNotification.put("title", "Google I/O 2016");
            if (CommonMethod.userName == null) CommonMethod.userName = "Someone";
            jNotification.put("body", CommonMethod.userName + " sends congratulations to you!");
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "OPEN_ACTIVITY_1");

            // If sending to a single client
            jPayload.put("to", toToken);
            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);



            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", FCMActivity.SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());


        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }


}

