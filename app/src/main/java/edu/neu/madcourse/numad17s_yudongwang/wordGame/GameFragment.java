/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material,
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose.
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
 ***/
package edu.neu.madcourse.numad17s_yudongwang.wordGame;

import android.app.Fragment;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashSet;
import java.util.Set;

import edu.neu.madcourse.numad17s_yudongwang.database.DatabaseAccess;
import edu.neu.madcourse.numad17s_yudongwang.R;


public class GameFragment extends Fragment {
    static private int mLargeIds[] = {R.id.large1, R.id.large2, R.id.large3,
            R.id.large4, R.id.large5, R.id.large6, R.id.large7, R.id.large8,
            R.id.large9,};
    static private int mSmallIds[] = {R.id.small1, R.id.small2, R.id.small3,
            R.id.small4, R.id.small5, R.id.small6, R.id.small7, R.id.small8,
            R.id.small9,};
    private Handler mHandler = new Handler();
    private Tile mEntireBoard = new Tile(this);
    private static Tile mLargeTiles[] = new Tile[9];
    private static Tile mSmallTiles[][] = new Tile[9][9];



    private Tile.Owner mPlayer = Tile.Owner.X;
    private Set<Tile> mAvailable = new HashSet<Tile>();
    private int mSoundX, mSoundO, mSoundMiss, mSoundRewind;
    private SoundPool mSoundPool;
    private float mVolume = 1f;
    private int mLastLarge;
    private int mLastSmall;


    private static String[] inputWords;
    private static Set<Tile>[] inputWordsPositions;
    private static int[] previous;
    private boolean stageTwo;
    private static String[] input;
    private static Set<String> userRightGuess;
    public static boolean store;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retain this fragment across configuration changes.
        setRetainInstance(true);
        initGame();

        mSoundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
        mSoundX = mSoundPool.load(getActivity(), R.raw.sergenious_movex, 1);
        mSoundO = mSoundPool.load(getActivity(), R.raw.sergenious_moveo, 1);
        mSoundMiss = mSoundPool.load(getActivity(), R.raw.erkanozan_miss, 1);
        mSoundRewind = mSoundPool.load(getActivity(), R.raw.joanne_rewind, 1);




    }


    public boolean isAvailable(Tile tile) {
        return mAvailable.contains(tile);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        View rootView =
                inflater.inflate(R.layout.large_board, container, false);



            initViews(rootView);
            updateAllTiles();





        return rootView;
    }

    public void changeStage() {
        for (int large = 0; large < 9; large++) {
            for (int small = 0; small < 9; small++) {
                mSmallTiles[large][small].removeInvalid();
                mSmallTiles[large][small].setUnSelected();
            }
        }


        for (int i = 0; i < 9; i++) {
            inputWords[i] = "";
            inputWordsPositions[i] = new HashSet<>();
            previous[i] = -1;
        }
        stageTwo = true;

    }


    private void initViews(View rootView) {
        mEntireBoard.setView(rootView);


        for (int large = 0; large < 9; large++) {
            View outer = rootView.findViewById(mLargeIds[large]);
            mLargeTiles[large].setView(outer);

            for (int small = 0; small < 9; small++) {
                final ImageButton inner = (ImageButton) outer.findViewById
                        (mSmallIds[small]);
                final int fLarge = large;
                final int fSmall = small;
                final Tile smallTile = mSmallTiles[large][small];
                smallTile.setView(inner);
                inner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        smallTile.animate();
                        mSoundPool.play(mSoundX, mVolume, mVolume, 1, 0, 1f);


                        if (!CommonMethod.enterStageTwo) {
                            stageOneGamePart(fLarge, fSmall, smallTile);
                        } else {
                            stageTwoGamePart(fLarge, fSmall, smallTile);
                        }


                    }
                });
            }
        }
        if (!CommonMethod.loadMode) fillWithWords();
    }

    private void stageTwoGamePart(int fLarge, int fSmall, Tile smallTile) {

        if (inputWordsPositions[0].contains(smallTile)) {
            for (Tile t : inputWordsPositions[0]) {
                t.setUnSelected();
            }
            updateAllTiles();
            inputWords[0] = "";
            inputWordsPositions[0].clear();
            mSoundPool.play(mSoundMiss, mVolume, mVolume, 1, 0, 1f);
            return;
        }
        if (smallTile.isNotEmpty()) inputWordsPositions[0].add(smallTile);
        makeMove(fLarge, fSmall);

        char curLetter = smallTile.getLetter();
        if (curLetter >= 'A' && curLetter <= 'Z') inputWords[0] += curLetter;


        final DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getActivity());
        databaseAccess.open();

        if (inputWords[0].length() >= 3 && curLetter >= 'A' && curLetter <= 'Z') {

            //check edu.neu.madcourse.numad17s_yudongwang.edu.neu.madcourse.numad17s_yudongwang.database----
            boolean res = databaseAccess.checkWord(inputWords[0].toLowerCase());


            if (res) {
                if (inputWordsPositions[0].size() < 9) {


                    //---
                    TextView textview = (TextView) getActivity().findViewById(R.id.score);
                    int count = Integer.valueOf(textview.getText().toString());
                    if (!userRightGuess.contains(inputWords[0].toUpperCase())) {
                        count += inputWords[0].length() * 100;
                        CommonMethod.stage2Score += inputWords[0].length() * 100;
                    }
                    CommonMethod.score = count;
                    textview.setText(String.valueOf(count));
                    //---
                    for (Tile t : inputWordsPositions[0]) {
                        t.setGoodGuess();
                    }
                } else {
                    //---
                    TextView textview = (TextView) getActivity().findViewById(R.id.score);
                    int count = Integer.valueOf(textview.getText().toString());
                    if (!userRightGuess.contains(inputWords[0].toUpperCase())) {
                        count = count + 2000;
                        CommonMethod.stage2Score += 2000;
                    }
                    CommonMethod.score = count;
                    textview.setText(String.valueOf(count));
                    //---
                    for (Tile t : inputWordsPositions[0]) {
                        t.setPerfectGuess();
                    }
                }
                userRightGuess.add(inputWords[0].toUpperCase());
            } else {
                if (inputWordsPositions[0].size() == 9) {
                    for (Tile t : inputWordsPositions[0]) {
                        t.setUnSelected();
                    }
                    updateAllTiles();
                    inputWords[fLarge] = "";
                    inputWordsPositions[0].clear();
                    mSoundPool.play(mSoundMiss, mVolume, mVolume, 1, 0, 1f);
                }

            }

        }
    }

    private void stageOneGamePart(int fLarge, int fSmall, Tile smallTile) {



        if (inputWordsPositions[fLarge].size() != 9 && validMove(fSmall, previous[fLarge]) &&
                !inputWordsPositions[fLarge].contains(smallTile)) {
            makeMove(fLarge, fSmall);
            previous[fLarge] = fSmall;
            inputWordsPositions[fLarge].add(smallTile);

            char curLetter = smallTile.getLetter();
            if (curLetter >= 'A' && curLetter <= 'Z') inputWords[fLarge] += curLetter;



            final DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getActivity());
            databaseAccess.open();
            if (inputWords[fLarge].length() >= 3) {

                //check edu.neu.madcourse.numad17s_yudongwang.edu.neu.madcourse.numad17s_yudongwang.database----
                boolean res = databaseAccess.checkWord(inputWords[fLarge].toLowerCase());
                if (res) {
                    if (inputWordsPositions[fLarge].size() < 9) {

                        //---
                        TextView textview = (TextView) getActivity().findViewById(R.id.score);
                        int count = Integer.valueOf(textview.getText().toString());
                        count = count + 50 * inputWords[fLarge].length();
                        CommonMethod.stage1Score = count;
                        CommonMethod.score = count;
                        textview.setText(String.valueOf(count));
                        //---
                        for (Tile t : inputWordsPositions[fLarge]) {
                            t.setGoodGuess();
                        }
                    } else {
                        //---
                        TextView textview = (TextView) getActivity().findViewById(R.id.score);
                        int count = Integer.valueOf(textview.getText().toString());
                        count = count + 1000;
                        CommonMethod.stage1Score = count;
                        CommonMethod.score = count;
                        textview.setText(String.valueOf(count));
                        //---
                        for (Tile t : inputWordsPositions[fLarge]) {
                            t.setPerfectGuess();
                        }
                    }
                    userRightGuess.add(inputWords[fLarge]);
                } else {
                    if (inputWordsPositions[fLarge].size() == 9) {
                        for (Tile t : inputWordsPositions[fLarge]) {
                            t.setUnSelected();
                        }
                        updateAllTiles();
                        inputWords[fLarge] = "";
                        inputWordsPositions[fLarge].clear();
                        mSoundPool.play(mSoundMiss, mVolume, mVolume, 1, 0, 1f);
                    }

                }


            }


        } else {

            //--- decrease score for wrong word
            TextView textview = (TextView) getActivity().findViewById(R.id.score);
            int count = Integer.valueOf(textview.getText().toString());
            count = count - 50 * inputWords[fLarge].length();
            if (count < 0) count = 0;
            CommonMethod.stage1Score -= 50 * inputWords[fLarge].length();
            if (CommonMethod.stage1Score < 0) CommonMethod.stage1Score = 0;
            CommonMethod.score = count;
            textview.setText(String.valueOf(count));
            //---
            previous[fLarge] = -1;





            for (Tile t : mSmallTiles[fLarge]) {
                t.setUnSelected();
            }
            updateAllTiles();
            inputWords[fLarge] = "";
            inputWordsPositions[fLarge].clear();
            mSoundPool.play(mSoundMiss, mVolume, mVolume, 1, 0, 1f);


        }
    }


    private boolean validMove(int next, int prev) {
        if (next == prev) return true;
        if (prev == -1) {
            return true;
        } else {
            if (prev == 0) return next == 1 || next == 3 || next == 4;
            if (prev == 1) return next == 0 || next == 2 || next == 3 || next == 4 || next == 5;
            if (prev == 2) return next == 1 || next == 4 || next == 5;
            if (prev == 3) return next == 0 || next == 1 || next == 4 || next == 6 || next == 7;
            if (prev == 4) return next == 0 || next == 1 || next == 2 || next == 3 || next == 5 ||
                    next == 6 || next == 7 || next == 8;
            if (prev == 5) return next == 1 || next == 2 || next == 4 || next == 7 || next == 8;
            if (prev == 6) return next == 3 || next == 4 || next == 7;
            if (prev == 7) return next == 3 || next == 4 || next == 5 || next == 6 || next == 8;
            if (prev == 8) return next == 4 || next == 5 || next == 7;
            return false;
        }
    }


    private void fillWithWords() {
        //the pattern is

        String[] pattern = {
                generateRandomPosition(), generateRandomPosition(), generateRandomPosition(),
                generateRandomPosition(), generateRandomPosition(), generateRandomPosition(),
                generateRandomPosition(), generateRandomPosition(), generateRandomPosition()};

        //the system select words from dictionary are


        final DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getActivity());
        databaseAccess.open();
        input = databaseAccess.randomSelectWord();


        for (int i = 0; i < 9; i++) {
            input[i] = input[i].toUpperCase();
        }

        CommonMethod.input = input;

        for (int large = 0; large < 9; large++) {
            for (int i = 0; i < 9; i++) {
                mSmallTiles[large][pattern[large].charAt(i) - '0'].setLetter(input[large].charAt(i));
            }
        }


    }


    public String[] getAnswer() {

        return CommonMethod.input;
    }

    public Set<String> getUserRightGuess() {
        return userRightGuess;
    }

    public static String generateRandomPosition() {

        String[] sum = {
                "301245876", "301246758", "301246785", "301248576", "301254678", "301254876", "301257648", "301257846", "301258467", "301258476", "301258746", "301258764", "301425876", "301467852", "301524678", "301524876", "301587642", "304125876", "304215876", "304678512", "304678521", "310425876", "310467852", "312587640", "340125876", "346785210", "364012578", "364012587", "364785210", "364875210", "367401258", "367485210", "367521048", "367584012", "367584210", "367840125", "367840152", "367842510", "367845210", "367851042", "367851240", "367852104", "367852140", "367852401", "367852410", "367854012", "367854210", "376401258", "376485210", "378521046", "401258736", "401258763", "401367852", "403125876", "403678512", "403678521", "410367852", "412587630", "421036758", "421036785", "421587630", "425103678", "425876301", "425876310", "430125876", "436785210", "452103678", "458763012", "463012578", "463012587", "463785210", "467301258", "467852103", "467852130", "476301258", "478521036", "485210367", "485210376", "485763012", "487521036", "487630125", "487630152", "510367842", "512403678", "512487630", "521034678", "521034876", "521036478", "521036487", "521036748", "521036784", "521037648", "521037846", "521043678", "521046378", "521048736", "521048763", "521304678", "521304876", "521367840", "521403678", "521487630", "524013678", "524103678", "524876301", "524876310", "542103678", "548763012", "576301248", "578421036", "578463012", "584210367", "584210376", "584673012", "584763012", "587301246", "587364012", "587364210", "587421036", "587463012", "587630124", "587630142", "587630412", "587630421", "587631042", "587631240", "587634012", "587634210", "587640312", "587642103", "587642130", "587643012", "630124578", "630124587", "630124758", "630124785", "630124857", "630124875", "630125478", "630125487", "630125748", "630125784", "630125847", "630125874", "630142578", "630142587", "630147852", "630148752", "630152478", "630152487", "630157842", "630158742", "630412578", "630412587", "630421578", "630421587", "630478512", "630478521", "630487512", "630487521", "631042578", "631042587", "631047852", "631048752", "631257840", "631258740", "634012578", "634012587", "634785210", "634875210", "637401258", "637485210", "637521048", "637584012", "637584210", "637840125", "637840152", "637842510", "637845210", "637851042", "637851240", "637852104", "637852140", "637852401", "637852410", "637854012", "637854210", "640125873", "640137852", "640312578", "640312587", "640378512", "640378521", "641037852", "641258730", "642103758", "642103785", "642158730", "642510378", "642587301", "642587310", "643012578", "643012587", "643785210", "645210378", "645873012", "647301258", "647852103", "647852130", "648521037", "648573012", "648730125", "648730152", "648752103", "648752130", "673012458", "673012485", "673012548", "673012584", "673014258", "673014852", "673015248", "673015842", "673041258", "673042158", "673048512", "673048521", "673104258", "673104852", "673125840", "673401258", "673485210", "674031258", "674301258", "674852103", "674852130", "675210348", "675213048", "675840312", "675842103", "675842130", "675843012", "678403125", "678403152", "678425103", "678425130", "678430125", "678430152", "678452103", "678452130", "678510342", "678512403", "678512430", "678513042", "678521034", "678521043", "678521304", "678521340", "678521403", "678521430", "678524013", "678524031", "678524103", "678524130", "678524301", "678524310", "678540312", "678542103", "678542130", "678543012", "840125736", "840125763", "840136752", "840312576", "840367512", "840367521", "841036752", "841257630", "842103675", "842157630", "842510367", "842510376", "842576301", "842576310", "843012576", "843675210", "845210367", "845210376", "845763012", "846301257", "846375210", "846730125", "846730152", "846752103", "846752130", "847521036", "847630125", "847630152", "851036742", "851037642", "851240367", "851240376", "851246730", "851247630", "852103467", "852103476", "852103647", "852103674", "852103746", "852103764", "852104367", "852104376", "852104637", "852104673", "852104736", "852104763", "852130467", "852130476", "852136740", "852137640", "852140367", "852140376", "852146730", "852147630", "852401367", "852401376", "852410367", "852410376", "852467301", "852467310", "852476301", "852476310", "854210367", "854210376", "854673012", "854763012", "857301246", "857364012", "857364210", "857421036", "857463012", "857630124", "857630142", "857630412", "857630421", "857631042", "857631240", "857634012", "857634210", "857640312", "857642103", "857642130", "857643012", "873012546", "873015246", "873640125", "873640152", "873642510", "873645210", "874251036", "874521036", "874630125", "874630152", "875103642", "875124036", "875124630", "875210346", "875210364", "875210436", "875210463", "875213046", "875213640", "875214036", "875214630", "875240136", "875241036", "875246301", "875246310", "875421036", "875463012", "876301245", "876301254", "876301425", "876301452", "876301524", "876301542", "876304125", "876304152", "876304215", "876304251", "876304512", "876304521", "876310425", "876310452", "876312540", "876315240", "876340125", "876340152", "876342510", "876345210", "876403125", "876403152", "876425103", "876425130", "876430125", "876430152", "876452103", "876452130", "841036752"
        };
        int pick = (int) (Math.random() * sum.length);
        String res = sum[pick];
        return res;
    }


    private void makeMove(int large, int small) {
        mLastLarge = large;
        mLastSmall = small;
        Tile smallTile = mSmallTiles[large][small];
        Tile largeTile = mLargeTiles[large];
//      smallTile.setOwner(mPlayer);
        smallTile.setSelected();
//      setAvailableFromLastMove(small);
        updateAllTiles();

    }

    public void restartGame() {
        mSoundPool.play(mSoundRewind, mVolume, mVolume, 1, 0, 1f);
        // ...

        if (!CommonMethod.enterStageTwo) {

            if (CommonMethod.resetTimes <= 0) {
                CharSequence text = "There is no remain reset left!";
                int duration = Toast.LENGTH_LONG;
                final Toast  toast = Toast.makeText(getActivity(), text, duration);
                toast.show();

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toast.cancel();
                    }
                }, 800);
            } else {
                for (int large = 0; large < 9; large++) {
                    for (int small = 0; small < 9; small++) {
                        mSmallTiles[large][small].setUnSelected();
                    }
                }


//            initGame();
                fillWithWords();
//            initViews(getView());
                updateAllTiles();
                CharSequence text = "You are reset the board, " + CommonMethod.resetTimes + " times left";
                CommonMethod.resetTimes--;

                int duration = Toast.LENGTH_LONG;
                final Toast  toast = Toast.makeText(getActivity(), text, duration);
                toast.show();

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toast.cancel();
                    }
                }, 800);

            }


        } else {
//            Toast.makeText(getActivity(), "test", Toast.LENGTH_SHORT).show();

            CharSequence text = "You can not reset the board in stage 2!";
            int duration = Toast.LENGTH_LONG;
            final Toast  toast = Toast.makeText(getActivity(), text, duration);
            toast.show();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toast.cancel();
                }
            }, 800);


        }


    }

    public void initGame() {
        if (!CommonMethod.loadMode) {
            inputWords = new String[9];
            inputWordsPositions = new Set[9];
            previous = new int[9];
            for (int i = 0; i < 9; i++) {
                inputWords[i] = "";
                inputWordsPositions[i] = new HashSet<>();
                previous[i] = -1;
            }
            userRightGuess = new HashSet<>();
        }


        stageTwo = false;




        Log.d("UT3", "init game");
        mEntireBoard = new Tile(this);
        // Create all the tiles
        for (int large = 0; large < 9; large++) {
            mLargeTiles[large] = new Tile(this);
            for (int small = 0; small < 9; small++) {
                mSmallTiles[large][small] = new Tile(this);

            }
            mLargeTiles[large].setSubTiles(mSmallTiles[large]);
        }

        mEntireBoard.setSubTiles(mLargeTiles);

        // If the player moves first, set which spots are available
        mLastSmall = -1;
        mLastLarge = -1;
        setAvailableFromLastMove(mLastSmall);
    }

    private void setAvailableFromLastMove(int small) {
    }

    private void updateAllTiles() {
        mEntireBoard.updateDrawableState();
        for (int large = 0; large < 9; large++) {
            mLargeTiles[large].updateDrawableState();
            for (int small = 0; small < 9; small++) {
                mSmallTiles[large][small].updateDrawableState();
            }
        }
    }

    public boolean getStore() {
        return store;
    }




    /**
     * Create a string containing the state of the game.
     */
    public String getState() {
        StringBuilder builder = new StringBuilder();
        builder.append(mLastLarge);
        builder.append(',');
        builder.append(mLastSmall);
        builder.append(',');
        for (int large = 0; large < 9; large++) {
            for (int small = 0; small < 9; small++) {
                builder.append(mSmallTiles[large][small].getOwner().name());
                builder.append(',');
                builder.append(mSmallTiles[large][small].getLetter());
                builder.append(',');
                builder.append(mSmallTiles[large][small].getValid());
                builder.append(',');

            }
        }
        return builder.toString();
    }

    /**
     * Restore the state of the game from the given string.
     */
    public void putState(String gameData) {
        String[] fields = gameData.split(",");
        int index = 0;
        mLastLarge = Integer.parseInt(fields[index++]);
        mLastSmall = Integer.parseInt(fields[index++]);
        for (int large = 0; large < 9; large++) {
            for (int small = 0; small < 9; small++) {
                Tile.Owner owner = Tile.Owner.valueOf(fields[index++]);
                mSmallTiles[large][small].setOwner(owner);
                Character letter = fields[index++].charAt(0);
                mSmallTiles[large][small].setLetter(letter);
                int valid = Integer.valueOf(fields[index++]);
                mSmallTiles[large][small].setValid(valid);
            }
        }
        setAvailableFromLastMove(mLastSmall);
        updateAllTiles();
    }
}

