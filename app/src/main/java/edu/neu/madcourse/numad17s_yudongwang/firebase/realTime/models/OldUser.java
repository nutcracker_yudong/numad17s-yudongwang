package edu.neu.madcourse.numad17s_yudongwang.firebase.realTime.models;

/**
 * Created by yudong on 3/2/17.
 */

public class OldUser {
    public String username;
    public String score;
    public String datePlayed;


    public OldUser() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public OldUser(String username, String score) {
        this.username = username;
        this.score = score;
        this.datePlayed = "2017-02-27";
    }

    public OldUser(String username, String score, String date) {
        this.username = username;
        this.score = score;
        this.datePlayed = date;
    }
}
