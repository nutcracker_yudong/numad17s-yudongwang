package edu.neu.madcourse.numad17s_yudongwang.wordGame;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


import edu.neu.madcourse.numad17s_yudongwang.*;
import edu.neu.madcourse.numad17s_yudongwang.firebase.fcm.FCMActivity;
import edu.neu.madcourse.numad17s_yudongwang.firebase.realTime.models.User;


import edu.neu.madcourse.numad17s_yudongwang.wordGame.CustomAdapter.*;

import static com.google.android.gms.internal.zzt.TAG;

public class LeaderBoard extends Activity{

    private List<Record> records;
    private Button btn_order_totalScore;
    private Button btn_order_wordScore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);




        records = new ArrayList<>();

        btn_order_totalScore = (Button) findViewById(R.id.btn_leaderboard_score);

        btn_order_totalScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadListByTotalScore();
            }
        });


        btn_order_wordScore = (Button) findViewById(R.id.btn_leaderboard_wordScore);

        btn_order_wordScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadListByWordScore();
            }
        });



        Record oneTest = new Record("loading", new User());
        records.add(oneTest);

        CustomAdapter adapter = new CustomAdapter(
                LeaderBoard.this,
                R.layout.mycustom_listitem,
                records);
        ListView customListView = (ListView) findViewById(R.id.LeaderBoard_itemList);
        customListView.setAdapter(adapter);
        loadListOnce();


    }


    public void loadListOnce() {


        // Get a reference to our posts
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("users");

        // Attach a listener to read the data at our posts reference
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                //--query
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                Query query = reference.child("users").orderByChild("score").limitToLast(10);
                query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            // dataSnapshot is the "issue" node with all children with id 0
                            records.clear();
                            for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                                // do something with the individual "issues"
                                User user = childDataSnapshot.getValue(User.class);
                                String record = "username: " + user.username + "\n" +
                                        "score: " + user.score + "\n" +
                                        "longest word: " + user.word + "\n" +
                                        "single word score: " + user.wordScore + "\n" +
                                        "date: " + user.datePlayed + "\n";
                                records.add(0, new Record(record, user));
                            }

                            //assign the adapter to listView

                            CustomAdapter adapter = new CustomAdapter(
                                    LeaderBoard.this,
                                    R.layout.mycustom_listitem,
                                    records);
                            ListView customListView = (ListView) findViewById(R.id.LeaderBoard_itemList);
                            customListView.setAdapter(adapter);

                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    public void loadListByTotalScore() {


        // Get a reference to our posts
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("users");

        // Attach a listener to read the data at our posts reference
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                //--query
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                Query query = reference.child("users").orderByChild("score").limitToLast(10);
                query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            // dataSnapshot is the "issue" node with all children with id 0
                            records.clear();

                            for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                                // do something with the individual "issues"
                                User user = childDataSnapshot.getValue(User.class);
                                String record = "username: " + user.username + "\n" +
                                        "score: " + user.score + "\n" +
                                        "longest word: " + user.word + "\n" +
                                        "single word score: " + user.wordScore + "\n" +
                                        "date: " + user.datePlayed + "\n";
                                records.add(0, new Record(record, user));
                            }

                            //assign the adapter to listView

                            CustomAdapter adapter = new CustomAdapter(
                                    LeaderBoard.this,
                                    R.layout.mycustom_listitem,
                                    records);
                            ListView customListView = (ListView) findViewById(R.id.LeaderBoard_itemList);
                            customListView.setAdapter(adapter);

                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    public void loadListByWordScore() {


        // Get a reference to our posts
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("users");

        // Attach a listener to read the data at our posts reference
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                //--query
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                Query query = reference.child("users").orderByChild("wordScore").limitToLast(10);
                query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            // dataSnapshot is the "issue" node with all children with id 0
                            records.clear();
                            for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                                // do something with the individual "issues"
                                User user = childDataSnapshot.getValue(User.class);
                                String record = "username: " + user.username + "\n" +
                                        "score: " + user.score + "\n" +
                                        "longest word: " + user.word + "\n" +
                                        "single word score: " + user.wordScore + "\n" +
                                        "date: " + user.datePlayed + "\n";
                                records.add(0, new Record(record, user));
                            }

                            //assign the adapter to listView

                            CustomAdapter adapter = new CustomAdapter(
                                    LeaderBoard.this,
                                    R.layout.mycustom_listitem,
                                    records);
                            ListView customListView = (ListView) findViewById(R.id.LeaderBoard_itemList);
                            customListView.setAdapter(adapter);

                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }




}
