package edu.neu.madcourse.numad17s_yudongwang.firebase.fcm;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Set;

import edu.neu.madcourse.numad17s_yudongwang.AboutActivity;
import edu.neu.madcourse.numad17s_yudongwang.R;
import edu.neu.madcourse.numad17s_yudongwang.TestDictionaryActivity;
import edu.neu.madcourse.numad17s_yudongwang.wordGame.CommonMethod;
import edu.neu.madcourse.numad17s_yudongwang.wordGame.GameActivity;
import edu.neu.madcourse.numad17s_yudongwang.wordGame.GameFragment;
import edu.neu.madcourse.numad17s_yudongwang.wordGame.LeaderBoard;
import edu.neu.madcourse.numad17s_yudongwang.wordGame.MainActivity;
import edu.neu.madcourse.numad17s_yudongwang.wordGame.Scoreboard;


public class WordGameMessagingService extends FirebaseMessagingService {
    private static final String TAG = WordGameMessagingService.class.getSimpleName();

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ


        Log.d(TAG, "From: " + remoteMessage.getFrom());


        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }



        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            if (CommonMethod.isPlaying) {
                sendNotification(remoteMessage.getNotification().getBody());
            } else {
                CommonMethod.message = remoteMessage.getNotification().getBody();
                Intent newPage = new Intent(WordGameMessagingService.this, edu.neu.madcourse.numad17s_yudongwang.wordGame.Dialog.class);
                newPage.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(newPage);
            }
        }


        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    public void sendDialog(String info) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String answerReport = "\n" + info + "\n";

        builder.setMessage(answerReport);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok_label,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        final Dialog dialog = builder.create();
        dialog.show();

    }


    public void onClick(View v) {
        new CustomTask().execute((Void[])null);
    }


    private class CustomTask extends AsyncTask<Void, Void, Void> {

        protected Void doInBackground(Void... param) {
            //Do some work
            return null;
        }

        protected void onPostExecute(Void param) {
            //Print Toast or open dialog
        }
    }


    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, LeaderBoard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.o_red)
                .setContentTitle("Class Message")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }




}