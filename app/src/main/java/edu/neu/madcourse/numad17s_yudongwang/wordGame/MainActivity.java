/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material,
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose.
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
 ***/
package edu.neu.madcourse.numad17s_yudongwang.wordGame;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import edu.neu.madcourse.numad17s_yudongwang.R;
import edu.neu.madcourse.numad17s_yudongwang.firebase.fcm.FCMActivity;

public class MainActivity extends Activity {
    MediaPlayer mMediaPlayer;

    private Switch mySwitch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ativity_main);
        mySwitch = (Switch) findViewById(R.id.music);

        CommonMethod.SoundOn();
        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.v("Switch State=", "" + isChecked);
                if (isChecked) {
                    CommonMethod.SoundOn();
                    CommonMethod.SoundPlayer(MainActivity.this, R.raw.a_guy_1_epicbuilduploop);

                } else {
                    CommonMethod.SoundStop();
                    CommonMethod.SoundMute();
                }
            }

        });

        //when the app runs, subscribe to news, so when new first appear it can receive
        FirebaseMessaging.getInstance().subscribeToTopic("news");


        String token = FirebaseInstanceId.getInstance().getToken();
        CommonMethod.token = token;
        // Log and toast

    }

    public void testMethod() {

    }

    @Override
    protected void onResume() {
        super.onResume();

        CommonMethod.SoundStop();
        CommonMethod.SoundPlayer(this, R.raw.a_guy_1_epicbuilduploop);
    }

    @Override
    protected void onPause() {
        super.onPause();

        CommonMethod.SoundStop();
    }
}
