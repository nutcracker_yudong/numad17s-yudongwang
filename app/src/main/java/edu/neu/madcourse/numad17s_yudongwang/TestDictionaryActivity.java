package edu.neu.madcourse.numad17s_yudongwang;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

import edu.neu.madcourse.numad17s_yudongwang.database.DatabaseAccess;


public class TestDictionaryActivity extends AppCompatActivity implements Serializable {

    private EditText myEditText;
    private ListView listView;
    public Button btn_return;
    public Button btn_knowledgement;
    public Button btn_clear;
    private ArrayList<String> content;
    private HashSet curContentSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);

        if (savedInstanceState == null) {
            content = new ArrayList<>();
            curContentSet = new HashSet<>();
        } else {
            content = savedInstanceState.getStringArrayList("dict");
            curContentSet = (HashSet) savedInstanceState.getSerializable("set");
        }


        btn_clear = (Button) findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content = new ArrayList<String>();
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                        getApplicationContext(),
                        R.layout.custom_listitem,
                        R.id.list_item,
                        content);
                //assign the adapter to listView
                listView.setAdapter(adapter);
                myEditText.setText("");
                curContentSet = new HashSet();
            }
        });


        btn_return = (Button) findViewById(R.id.btn2);
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newPage = new Intent(TestDictionaryActivity.this, MainActivity.class);
                finish();
            }
        });


        btn_knowledgement = (Button) findViewById(R.id.btn3);
        btn_knowledgement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(TestDictionaryActivity.this).create();
                alertDialog.setTitle("Knowledgement");
                alertDialog.setMessage("Sqlite from android developer document\n" +
                        "https://developer.android.com/training/basics/data-storage/databases.html?hl=zh-eg" +
                        "\n\nI tried three way to finish this assignment." +
                        " Reading directly from txt, use deserialize, use " +
                        "sqlite.\nI choose the last method because the big data could be " +
                        "well managed by the sqlite and for future improvement.\nAlso this " +
                        "app can maintain the list when rotate the screen");


                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        });


        final DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        myEditText = (EditText) findViewById(R.id.editText2);
        myEditText.getText();
        myEditText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {


                long tStart = System.nanoTime();

                databaseAccess.open();

                if (s.toString().length() >= 3 && databaseAccess.checkWord(s.toString())) {

                    if (!curContentSet.contains(s.toString())) {
                        try {
                            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                            r.play();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        content.add(s.toString());
                    }
                    curContentSet.add(s.toString());
                }
//                databaseAccess.close();

                long tEnd = System.nanoTime();
                long tRes = tEnd - tStart; // time in nanoseconds

            }
        });


        listView = (ListView) findViewById(R.id.list);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getApplicationContext(),
                R.layout.custom_listitem,
                R.id.list_item,
                content);
        //assign the adapter to listView
        listView.setAdapter(adapter);


    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putStringArrayList("dict", content);
        savedInstanceState.putSerializable("set", curContentSet);
    }

}
