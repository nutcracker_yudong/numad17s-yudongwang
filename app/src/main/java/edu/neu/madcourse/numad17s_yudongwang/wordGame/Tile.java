/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material,
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose.
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
 ***/
package edu.neu.madcourse.numad17s_yudongwang.wordGame;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageButton;

import java.util.Arrays;

import edu.neu.madcourse.numad17s_yudongwang.R;

import static android.graphics.Color.GREEN;
import static android.graphics.Color.YELLOW;

public class Tile {

    public enum Owner {
        NEITHER, BOTH, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
        AZ,BZ,CZ,DZ,EZ,FZ,GZ,HZ,IZ,JZ,KZ,LZ,MZ,NZ,OZ,PZ,QZ,RZ,SZ,TZ,UZ,VZ,WZ,XZ,YZ,ZZ
    }

    // These levels are defined in the drawable definitions
    private static final int LEVEL_XX = 27;
    private static final int LEVEL_OO = 28; // letter O
    private static final int LEVEL_BLANK = 29;
    private static final int LEVEL_AVAILABLE = 30;
    private static final int LEVEL_TIE = 30;

    private static final int LEVEL_A = 1;
    private static final int LEVEL_B = 2;
    private static final int LEVEL_C = 3;
    private static final int LEVEL_D = 4;
    private static final int LEVEL_E = 5;
    private static final int LEVEL_F = 6;
    private static final int LEVEL_G = 7;
    private static final int LEVEL_H = 8;
    private static final int LEVEL_I = 9;
    private static final int LEVEL_J = 10;
    private static final int LEVEL_K = 11;
    private static final int LEVEL_L = 12;
    private static final int LEVEL_M = 13;
    private static final int LEVEL_N = 14;
    private static final int LEVEL_O = 15;
    private static final int LEVEL_P = 16;
    private static final int LEVEL_Q = 17;
    private static final int LEVEL_R = 18;
    private static final int LEVEL_S = 19;
    private static final int LEVEL_T = 20;
    private static final int LEVEL_U = 21;
    private static final int LEVEL_V = 22;
    private static final int LEVEL_W = 23;
    private static final int LEVEL_X = 24;
    private static final int LEVEL_Y = 25;
    private static final int LEVEL_Z = 26;
    private static final int LEVEL_AZ = 31;
    private static final int LEVEL_BZ = 32;
    private static final int LEVEL_CZ = 33;
    private static final int LEVEL_DZ = 34;
    private static final int LEVEL_EZ = 35;
    private static final int LEVEL_FZ = 36;
    private static final int LEVEL_GZ = 37;
    private static final int LEVEL_HZ = 38;
    private static final int LEVEL_IZ = 39;
    private static final int LEVEL_JZ = 40;
    private static final int LEVEL_KZ = 41;
    private static final int LEVEL_LZ = 42;
    private static final int LEVEL_MZ = 43;
    private static final int LEVEL_NZ = 44;
    private static final int LEVEL_OZ = 45;
    private static final int LEVEL_PZ = 46;
    private static final int LEVEL_QZ = 47;
    private static final int LEVEL_RZ = 48;
    private static final int LEVEL_SZ = 49;
    private static final int LEVEL_TZ = 50;
    private static final int LEVEL_UZ = 51;
    private static final int LEVEL_VZ = 52;
    private static final int LEVEL_WZ = 53;
    private static final int LEVEL_XZ = 54;
    private static final int LEVEL_YZ = 55;
    private static final int LEVEL_ZZ = 56;



    private final GameFragment mGame;
    public Owner mOwner = Owner.NEITHER;
    public char letter;
    private View mView;
    private Tile mSubTiles[];
    private int valid;



    public Tile(GameFragment game) {
        this.mGame = game;
    }

    public Tile deepCopy() {
        Tile tile = new Tile(mGame);
        tile.setOwner(getOwner());
        if (getSubTiles() != null) {
            Tile newTiles[] = new Tile[9];
            Tile oldTiles[] = getSubTiles();
            for (int child = 0; child < 9; child++) {
                newTiles[child] = oldTiles[child].deepCopy();
            }
            tile.setSubTiles(newTiles);
        }
        return tile;
    }

    public Character getLetter() {
        return letter;
    }

    public void setLetter(Character letter) {
        this.valid = 0;
        this.letter = letter;
        if (letter == 'A') setOwner(Owner.A);
        if (letter == 'B') setOwner(Owner.B);
        if (letter == 'C') setOwner(Owner.C);
        if (letter == 'D') setOwner(Owner.D);
        if (letter == 'E') setOwner(Owner.E);
        if (letter == 'F') setOwner(Owner.F);
        if (letter == 'G') setOwner(Owner.G);
        if (letter == 'H') setOwner(Owner.H);
        if (letter == 'I') setOwner(Owner.I);
        if (letter == 'J') setOwner(Owner.J);
        if (letter == 'K') setOwner(Owner.K);
        if (letter == 'L') setOwner(Owner.L);
        if (letter == 'M') setOwner(Owner.M);
        if (letter == 'N') setOwner(Owner.N);
        if (letter == 'O') setOwner(Owner.O);
        if (letter == 'P') setOwner(Owner.P);
        if (letter == 'Q') setOwner(Owner.Q);
        if (letter == 'R') setOwner(Owner.R);
        if (letter == 'S') setOwner(Owner.S);
        if (letter == 'T') setOwner(Owner.T);
        if (letter == 'U') setOwner(Owner.U);
        if (letter == 'V') setOwner(Owner.V);
        if (letter == 'W') setOwner(Owner.W);
        if (letter == 'X') setOwner(Owner.X);
        if (letter == 'Y') setOwner(Owner.Y);
        if (letter == 'Z') setOwner(Owner.Z);
    }

    public View getView() {
        return mView;
    }

    public void setView(View view) {
        this.mView = view;
    }

    public Owner getOwner() {
        return mOwner;
    }

    public void setOwner(Owner owner) {

        this.mOwner = owner;

    }

    public void removeInvalid() {
        if(valid == 0) {
            setOwner(Owner.NEITHER);
            setLetter(' ');
            updateDrawableState();
        }
        getView().setBackgroundColor(View.GONE);
    }

    public void setUnSelected() {
        if (letter == 'A') setOwner(Owner.A);
        if (letter == 'B') setOwner(Owner.B);
        if (letter == 'C') setOwner(Owner.C);
        if (letter == 'D') setOwner(Owner.D);
        if (letter == 'E') setOwner(Owner.E);
        if (letter == 'F') setOwner(Owner.F);
        if (letter == 'G') setOwner(Owner.G);
        if (letter == 'H') setOwner(Owner.H);
        if (letter == 'I') setOwner(Owner.I);
        if (letter == 'J') setOwner(Owner.J);
        if (letter == 'K') setOwner(Owner.K);
        if (letter == 'L') setOwner(Owner.L);
        if (letter == 'M') setOwner(Owner.M);
        if (letter == 'N') setOwner(Owner.N);
        if (letter == 'O') setOwner(Owner.O);
        if (letter == 'P') setOwner(Owner.P);
        if (letter == 'Q') setOwner(Owner.Q);
        if (letter == 'R') setOwner(Owner.R);
        if (letter == 'S') setOwner(Owner.S);
        if (letter == 'T') setOwner(Owner.T);
        if (letter == 'U') setOwner(Owner.U);
        if (letter == 'V') setOwner(Owner.V);
        if (letter == 'W') setOwner(Owner.W);
        if (letter == 'X') setOwner(Owner.X);
        if (letter == 'Y') setOwner(Owner.Y);
        if (letter == 'Z') setOwner(Owner.Z);
        getView().setBackgroundColor(View.GONE);
        this.valid = 0;
    }

    public void setSelected() {

        if (letter == 'A') setOwner(Owner.AZ);
        if (letter == 'B') setOwner(Owner.BZ);
        if (letter == 'C') setOwner(Owner.CZ);
        if (letter == 'D') setOwner(Owner.DZ);
        if (letter == 'E') setOwner(Owner.EZ);
        if (letter == 'F') setOwner(Owner.FZ);
        if (letter == 'G') setOwner(Owner.GZ);
        if (letter == 'H') setOwner(Owner.HZ);
        if (letter == 'I') setOwner(Owner.IZ);
        if (letter == 'J') setOwner(Owner.JZ);
        if (letter == 'K') setOwner(Owner.KZ);
        if (letter == 'L') setOwner(Owner.LZ);
        if (letter == 'M') setOwner(Owner.MZ);
        if (letter == 'N') setOwner(Owner.NZ);
        if (letter == 'O') setOwner(Owner.OZ);
        if (letter == 'P') setOwner(Owner.PZ);
        if (letter == 'Q') setOwner(Owner.QZ);
        if (letter == 'R') setOwner(Owner.RZ);
        if (letter == 'S') setOwner(Owner.SZ);
        if (letter == 'T') setOwner(Owner.TZ);
        if (letter == 'U') setOwner(Owner.UZ);
        if (letter == 'V') setOwner(Owner.VZ);
        if (letter == 'W') setOwner(Owner.WZ);
        if (letter == 'X') setOwner(Owner.XZ);
        if (letter == 'Y') setOwner(Owner.YZ);
        if (letter == 'Z') setOwner(Owner.ZZ);
    }

    public void setGoodGuess() {
        getView().setBackgroundColor(YELLOW);
        valid = 1;
    }

    public void setPerfectGuess() {
        getView().setBackgroundColor(GREEN);
        valid = 2;
    }

    public Tile[] getSubTiles() {
        return mSubTiles;
    }

    public void setSubTiles(Tile[] subTiles) {
        this.mSubTiles = subTiles;
    }

    public void updateDrawableState() {
        if (mView == null) return;
        int level = getLevel();
        if (mView.getBackground() != null) {
            mView.getBackground().setLevel(level);
        }
        if (mView instanceof ImageButton) {
            Drawable drawable = ((ImageButton) mView).getDrawable();
            drawable.setLevel(level);
        }
        if (valid == 1) setGoodGuess();
        if (valid == 2) setPerfectGuess();
    }

    private int getLevel() {
        int level = LEVEL_BLANK;
        switch (mOwner) {

            case A:
                level = LEVEL_A;
                break;
            case B:
                level = LEVEL_B;
                break;
            case C:
                level = LEVEL_C;
                break;
            case D:
                level = LEVEL_D;
                break;
            case E:
                level = LEVEL_E;
                break;
            case F:
                level = LEVEL_F;
                break;
            case G:
                level = LEVEL_G;
                break;
            case H:
                level = LEVEL_H;
                break;
            case I:
                level = LEVEL_I;
                break;
            case J:
                level = LEVEL_J;
                break;
            case K:
                level = LEVEL_K;
                break;
            case L:
                level = LEVEL_L;
                break;
            case M:
                level = LEVEL_M;
                break;
            case N:
                level = LEVEL_N;
                break;
            case O:
                level = LEVEL_O;
                break;
            case P:
                level = LEVEL_P;
                break;
            case Q:
                level = LEVEL_Q;
                break;
            case R:
                level = LEVEL_R;
                break;
            case S:
                level = LEVEL_S;
                break;
            case T:
                level = LEVEL_T;
                break;
            case U:
                level = LEVEL_U;
                break;
            case V:
                level = LEVEL_V;
                break;
            case W:
                level = LEVEL_W;
                break;
            case X:
                level = LEVEL_X;
                break;
            case Y:
                level = LEVEL_Y;
                break;
            case Z:
                level = LEVEL_Z;
                break;
            case AZ:
                level = LEVEL_AZ;
                break;
            case BZ:
                level = LEVEL_BZ;
                break;
            case CZ:
                level = LEVEL_CZ;
                break;
            case DZ:
                level = LEVEL_DZ;
                break;
            case EZ:
                level = LEVEL_EZ;
                break;
            case FZ:
                level = LEVEL_FZ;
                break;
            case GZ:
                level = LEVEL_GZ;
                break;
            case HZ:
                level = LEVEL_HZ;
                break;
            case IZ:
                level = LEVEL_IZ;
                break;
            case JZ:
                level = LEVEL_JZ;
                break;
            case KZ:
                level = LEVEL_KZ;
                break;
            case LZ:
                level = LEVEL_LZ;
                break;
            case MZ:
                level = LEVEL_MZ;
                break;
            case NZ:
                level = LEVEL_NZ;
                break;
            case OZ:
                level = LEVEL_OZ;
                break;
            case PZ:
                level = LEVEL_PZ;
                break;
            case QZ:
                level = LEVEL_QZ;
                break;
            case RZ:
                level = LEVEL_RZ;
                break;
            case SZ:
                level = LEVEL_SZ;
                break;
            case TZ:
                level = LEVEL_TZ;
                break;
            case UZ:
                level = LEVEL_UZ;
                break;
            case VZ:
                level = LEVEL_VZ;
                break;
            case WZ:
                level = LEVEL_WZ;
                break;
            case XZ:
                level = LEVEL_XZ;
                break;
            case YZ:
                level = LEVEL_YZ;
                break;
            case ZZ:
                level = LEVEL_ZZ;
                break;


            case BOTH:
                level = LEVEL_TIE;
                break;
            case NEITHER:
                level = mGame.isAvailable(this) ? LEVEL_AVAILABLE : LEVEL_BLANK;
                break;
        }
        return level;
    }

    public String getValid() {
        return this.valid + "";
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public boolean isNotEmpty() {
        return letter != ' ';
    }

    public void animate() {
        Animator anim = AnimatorInflater.loadAnimator(mGame.getActivity(),
                R.animator.tictactoe);
        if (getView() != null) {
            anim.setTarget(getView());
            anim.start();
        }
    }

    @Override
    public String toString() {
        return "Tile{" +
                "mGame=" + mGame +
                ", mOwner=" + mOwner +
                ", letter=" + letter +
                ", mView=" + mView +
                ", mSubTiles=" + Arrays.toString(mSubTiles) +
                ", valid=" + valid +
                '}';
    }
}
