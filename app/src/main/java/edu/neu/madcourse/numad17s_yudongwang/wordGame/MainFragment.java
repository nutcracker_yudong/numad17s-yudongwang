/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material,
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose.
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
 ***/
package edu.neu.madcourse.numad17s_yudongwang.wordGame;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import edu.neu.madcourse.numad17s_yudongwang.R;

public class MainFragment extends Fragment {

    private AlertDialog mDialog;

    private Button newButton;
    private Button continueButton;
    private Button aboutButton;
    private Button btn_scoreBoard;
    private Button btn_learderBoard;
    private EditText userName;

    SharedPreferences shared_preferences;
    SharedPreferences.Editor shared_preferences_editor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        final View rootView =
                inflater.inflate(R.layout.fragment_main, container, false);
        // Handle buttons here...
        newButton = (Button) rootView.findViewById(R.id.new_button);
        continueButton = (Button) rootView.findViewById(R.id.continue_button);
        aboutButton = (Button) rootView.findViewById(R.id.about_button);
        btn_scoreBoard = (Button) rootView.findViewById(R.id.btn_scoreBoard);
        btn_learderBoard = (Button) rootView.findViewById(R.id.btn_leaderBoard);
        userName = (EditText) rootView.findViewById(R.id.text_username);


        if (!CommonMethod.currentGame) {
            continueButton.setVisibility(View.GONE);
        }

        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GameActivity.class);
                CommonMethod.loadMode = false;
                CommonMethod.currentGame = true;
                CommonMethod.enterStageTwo = false;
                CommonMethod.resetTimes = 3;
                CommonMethod.stage1Score = 0;
                CommonMethod.stage2Score = 0;
                CommonMethod.score = 0;
                CommonMethod.wordHighestScore = 0;
                CommonMethod.isPlaying = true;
                getActivity().startActivity(intent);
            }
        });

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GameActivity.class);
                intent.putExtra(GameActivity.KEY_RESTORE, true);
                CommonMethod.loadMode = true;
                CommonMethod.isPlaying = true;
                getActivity().startActivity(intent);

            }
        });

        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.about_text);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // nothing
                            }
                        });
                mDialog = builder.show();
            }
        });


        btn_scoreBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Scoreboard.class);
                getActivity().startActivity(intent);
            }
        });

        btn_learderBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), LeaderBoard.class);
                getActivity().startActivity(intent);
            }
        });


        userName.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                CommonMethod.userName = s.toString();
                shared_preferences = PreferenceManager.getDefaultSharedPreferences(rootView.getContext());
                shared_preferences_editor = shared_preferences.edit();
                shared_preferences_editor.putString("username", s.toString());
                shared_preferences_editor.commit();

            }
        });


        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();

        // Get rid of the about dialog if it's still up
        if (mDialog != null)
            mDialog.dismiss();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (CommonMethod.currentGame) {
            continueButton.setVisibility(View.VISIBLE);
        } else {
            continueButton.setVisibility(View.GONE);
        }
    }

}
