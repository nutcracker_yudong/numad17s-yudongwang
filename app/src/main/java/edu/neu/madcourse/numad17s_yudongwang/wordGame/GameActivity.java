/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material,
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose.
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
 ***/
package edu.neu.madcourse.numad17s_yudongwang.wordGame;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;
import java.util.Set;
import java.util.TimeZone;

import edu.neu.madcourse.numad17s_yudongwang.R;
import edu.neu.madcourse.numad17s_yudongwang.firebase.fcm.FCMActivity;
import edu.neu.madcourse.numad17s_yudongwang.firebase.realTime.models.User;

import static android.text.format.Time.getCurrentTimezone;
import static com.google.android.gms.internal.zzt.TAG;

public class GameActivity extends Activity {
    public static final String KEY_RESTORE = "key_restore";
    public static final String PREF_RESTORE = "pref_restore";
    private MediaPlayer mMediaPlayer;
    private Handler mHandler = new Handler();
    private GameFragment mGameFragment;
    private boolean enterStage2;
    private CountDownTimer countDownTimer;
    private boolean musicOn;


    private Button btn_pause;
    private CountDownTimer timer;
    public long milliLeft;
    private long min;
    private long sec;
    TextView userScore;
    TextView userTime;


    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;

    SharedPreferences shared_preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        mGameFragment = (GameFragment) getFragmentManager()
                .findFragmentById(R.id.fragment_game);

        boolean restore = getIntent().getBooleanExtra(KEY_RESTORE, false);


        userScore = (TextView) findViewById(R.id.score);
        userTime = (TextView) findViewById(R.id.countdownTimer);
        btn_pause = (Button) findViewById(R.id.btn_pause);
        enterStage2 = false;

        if (restore) {
            String gameData = getPreferences(MODE_PRIVATE)
                    .getString(PREF_RESTORE, null);

            if (gameData != null) {
                mGameFragment.putState(gameData);
            }
            timerStart(CommonMethod.time);
            userScore.setText(String.valueOf(CommonMethod.score));
        } else {
            timerStart(90000);
        }


        btn_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btn_pause.getText().equals("Pause")) {
                    Log.i("Paused", btn_pause.getText().toString());
                    btn_pause.setText("Resume");
                    timerPause();
                    CommonMethod.SoundMyPause();
                    mGameFragment.getView().setVisibility(View.GONE);

                } else if (btn_pause.getText().equals("Resume")) {
                    btn_pause.setText("Pause");
                    timerResume();
                    CommonMethod.SoundResume();
                    mGameFragment.getView().setVisibility(View.VISIBLE);
                }

            }
        });

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {
                /*
                 * The following method, "handleShakeEvent(count):" is a stub //
				 * method you would use to setup whatever you want done once the
				 * device has been shook.
				 */
                restartGame();
            }
        });


//        Log.d("UT3", "restore = " + restore);
    }


    public void restartGame() {
        mGameFragment.restartGame();
    }

    public void reportWinner(final Tile.Owner winner, TextView userScore) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        CommonMethod.SoundPlayer(GameActivity.this, R.raw.department64_draw);

        String[] answer = ((GameFragment) getFragmentManager().findFragmentById(R.id.fragment_game)).getAnswer();
        Set<String> userRightGuess = ((GameFragment) getFragmentManager().findFragmentById(R.id.fragment_game)).getUserRightGuess();
        String answerReport = "\n" + "The correct answers are : \n " + "\n";
        int index = 0;
        for (String s : answer) {
            answerReport += index + " " + s.toLowerCase() + "\n";
            index++;
        }
        String userGuessReport = "You right guess are : \n ";
        for (String s : userRightGuess) {
            userGuessReport += "\n" + s.toLowerCase();
        }
        String output = "Game End! Your score is " + userScore.getText().toString() + " \n"
                + "stage1 score : " + CommonMethod.stage1Score + "\n"
                + "stage2 score : " + CommonMethod.stage2Score + "\n"
                + userGuessReport + "\n"
                + answerReport;
        builder.setMessage(output);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok_label,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        final Dialog dialog = builder.create();
        dialog.show();


        updateScoreBoard(userScore.getText().toString(), userRightGuess);
        //the leader board must be latter than the scoreBoard
        updateLeaderBoard();

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                CommonMethod.SoundPlayer(GameActivity.this, R.raw.department64_draw);

            }
        }, 500);

        CommonMethod.currentGame = false;

        // Reset the board to the initial position
//        mGameFragment.initGame();
    }

    private void updateLeaderBoard() {
        uploadToRealTimeDatabase();
        beatRecord();

    }

    private boolean beatRecord() {


        FirebaseDatabase database = FirebaseDatabase.getInstance();

        // Get a reference to our users
        DatabaseReference ref = database.getReference("users");

        // Attach a listener to read the data at our posts reference
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {



                //--query
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                Query query = reference.child("users").orderByChild("score").limitToLast(1);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            // dataSnapshot is the "issue" node with all children with id 0

                            for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                                // do something with the individual "issues"
                                User user = childDataSnapshot.getValue(User.class);
                                if (Integer.valueOf(CommonMethod.score) >= Integer.valueOf(user.score)) {
                                    pushNotification();
                                } else {
                                    Toast.makeText(GameActivity.this, "This is lower than the first one " + user.score, Toast.LENGTH_LONG).show();
                                }
                            }
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        return true;
    }

    private void uploadToRealTimeDatabase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mDatabase = database.getReference("users");
        if (CommonMethod.userName == null || CommonMethod.userName.equals("")) CommonMethod.userName = "defaultUser";
        User user = new User(CommonMethod.userName, CommonMethod.score, CommonMethod.longestWord,
                CommonMethod.wordHighestScore, CommonMethod.timePlayed, CommonMethod.token);
        mDatabase.child(user.datePlayed).setValue(user);
    }


    public void pushNotification() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                anotherThreadNotification();
            }
        }).start();
    }

    public void anotherThreadNotification() {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        try {
            jNotification.put("title", "Google I/O 2016");
            jNotification.put("body", "There is a new winner with higher score!");
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "OPEN_ACTIVITY_1");

            // If sending to a single client
            jPayload.put("to", "/topics/news");

            /*
            // If sending to multiple clients (must be more than 1 and less than 1000)
            JSONArray ja = new JSONArray();
            ja.put(CLIENT_REGISTRATION_TOKEN);
            // Add Other client tokens
            ja.put(FirebaseInstanceId.getInstance().getToken());
            jPayload.put("registration_ids", ja);
            */

            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);


//
//
//            https://fcm.googleapis.com/fcm/send
//              Content-Type:application/json
//              Authorization:key=AIzaSyZ-1u...0GBYzPu7Udno5aA
//              {
//                   "to": "/topics/foo-bar",
//                   "data": {
//                     "message": "This is a Firebase Cloud Messaging Topic Message!",
//                }
//              }

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", FCMActivity.SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());


            h.post(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "run: " + resp);
                    Toast.makeText(GameActivity.this, "There is a new winner in the leaderboard", Toast.LENGTH_LONG).show();
                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }






    private void updateScoreBoard(String s, Set<String> userRightGuess) {
        String longestWord = "";
        for (String guess : userRightGuess) {
            if (guess.length() > longestWord.length()) {
                longestWord = guess;
            }
        }
        CommonMethod.longestWord = longestWord;

        SharedPreferences shared_preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor shared_preferences_editor = shared_preferences.edit();


        DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm:ss:SSSSSS");
        df.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        String date = df.format(Calendar.getInstance().getTime());
        CommonMethod.timePlayed = date;
        CommonMethod.wordHighestScore = CommonMethod.longestWord.length() * 50;

        String scoreRecord = "Time played : " + CommonMethod.timePlayed + " \n" +
                "Final game score : " + CommonMethod.score + " \n" +
                "Phase 1 score : " + CommonMethod.stage1Score + " \n" +
                "Phase 2 score : " + CommonMethod.stage2Score + " \n" +
                "Word : " + CommonMethod.longestWord + " \n" +
                "Word Highest score (double is Phase 2): " + CommonMethod.wordHighestScore;

        String previous_string = shared_preferences.getString("scoreboard", "Default");

        if (previous_string.isEmpty()) {
            shared_preferences_editor.putString("scoreboard", scoreRecord);
        } else {
            shared_preferences_editor.putString("scoreboard", previous_string + "$" + scoreRecord);
        }

        shared_preferences_editor.apply();

    }


    public void timerStart(long timeLengthMilli) {


        timer = new CountDownTimer(timeLengthMilli, 1000) {

            @Override
            public void onTick(long milliTillFinish) {
                milliLeft = milliTillFinish;
                min = (milliTillFinish / (1000 * 60));
                sec = ((milliTillFinish / 1000) - min * 60);
                userTime.setText(Long.toString(min) + ":" + Long.toString(sec));
                if (4000 < milliTillFinish && milliTillFinish < 5000) {

                    CommonMethod.SoundPlayer(GameActivity.this, R.raw.timeup);

                }
            }

            @Override
            public void onFinish() {

                userTime.setText("stage 1 finish!");
                mGameFragment.changeStage();

                if (!CommonMethod.enterStageTwo) {
                    CommonMethod.SoundPlayer(GameActivity.this, R.raw.stage2);
                    enterStage2 = true;
                    CommonMethod.enterStageTwo = true;
                    timerStart(60000);
                } else {
                    userTime.setText("finish!");
                    reportWinner(Tile.Owner.A, userScore);
                    enterStage2 = false;
                    CommonMethod.enterStageTwo = false;
                }
            }
        }.start();
    }


    public void timerPause() {
        timer.cancel();
    }

    private void timerResume() {
        Log.i("min", Long.toString(min));
        Log.i("Sec", Long.toString(sec));
        timerStart(milliLeft);
    }


    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }


    @Override
    protected void onResume() {

        super.onResume();
        if (!enterStage2) {
            CommonMethod.SoundPlayer(this, R.raw.stage1);
            CommonMethod.getMusicState();
        } else {
            CommonMethod.SoundPlayer(this, R.raw.stage2);
            CommonMethod.getMusicState();

        }
        // Add the following line to register the Session Manager Listener onResume
        mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);

    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacks(null);
        timer.cancel();
        timerPause();
        CommonMethod.SoundStop();

        String gameData = mGameFragment.getState();
        getPreferences(MODE_PRIVATE).edit()
                .putString(PREF_RESTORE, gameData)
                .apply();
//        Log.d("UT3", "state = " + gameData);
        // Add the following line to unregister the Sensor Manager onPause
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
        CommonMethod.time = milliLeft;
        CommonMethod.score = Integer.valueOf(userScore.getText().toString());
        CommonMethod.isPlaying = false;
    }
}
