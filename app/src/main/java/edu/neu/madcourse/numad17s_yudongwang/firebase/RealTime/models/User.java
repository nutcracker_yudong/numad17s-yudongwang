package edu.neu.madcourse.numad17s_yudongwang.firebase.realTime.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by aniru on 2/18/2017.
 */
@IgnoreExtraProperties
public class User {

    public String username;
    public Integer score;
    public String word;
    public Integer wordScore;
    public String datePlayed;
    public String token;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String username, Integer score, String word, Integer wordScore, String datePlayed, String token) {
        this.username = username;
        this.score = score;
        this.word = word;
        this.wordScore = wordScore;
        this.datePlayed = datePlayed;
        this.token = token;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", score='" + score + '\'' +
                ", word='" + word + '\'' +
                ", wordScore='" + wordScore + '\'' +
                ", datePlayed='" + datePlayed + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}