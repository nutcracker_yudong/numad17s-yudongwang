package edu.neu.madcourse.numad17s_yudongwang.wordGame;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import edu.neu.madcourse.numad17s_yudongwang.R;


/**
 * Created by yudong on 3/4/17.
 */

public class Dialog extends Activity {
    TextView messge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dialog);
        messge = (TextView) findViewById(edu.neu.madcourse.numad17s_yudongwang.R.id.textView_dialog);
        messge.setText(CommonMethod.message);
    }
}
