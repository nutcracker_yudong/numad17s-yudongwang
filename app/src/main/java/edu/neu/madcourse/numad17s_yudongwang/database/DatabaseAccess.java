package edu.neu.madcourse.numad17s_yudongwang.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the edu.neu.madcourse.numad17s_yudongwang.edu.neu.madcourse.numad17s_yudongwang.database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the edu.neu.madcourse.numad17s_yudongwang.edu.neu.madcourse.numad17s_yudongwang.database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    public boolean checkWord(String input) {
        Cursor dataCount = database.rawQuery("select * from wordlist where wordDictionary =?", new String[]{input});
        return dataCount.getCount() == 1;
    }


    public String[] randomSelectWord() {
        String[] res = new String[9];
        Cursor cursor = database.rawQuery("SELECT * FROM wordlist WHERE LENGTH(wordDictionary) = 9 ORDER BY RANDOM() LIMIT 9;", new String[]{});
        int index = 0;
        cursor.moveToFirst();
        do {
            res[index++] = (cursor.getString(0));
        } while (cursor.moveToNext());
        cursor.close();
        return res;
    }
}
