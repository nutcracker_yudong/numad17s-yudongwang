package edu.neu.madcourse.numad17s_yudongwang;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.Serializable;

import edu.neu.madcourse.numad17s_yudongwang.firebase.fcm.FCMActivity;

public class MainActivity extends AppCompatActivity implements Serializable{

    public Button but1;
    public Button but2;
    public Button but3;
    public Button but4;


    public void init() {
        but1 = (Button) findViewById(R.id.but1);
        but1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent newPage = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(newPage);
            }
        });

        but2 = (Button) findViewById(R.id.btn2);
        but2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(5 / 0);
            }
        });

        but3 = (Button) findViewById(R.id.but3);
        but3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent newPage = new Intent(MainActivity.this, TestDictionaryActivity.class);
                startActivity(newPage);
            }
        });

        but4 = (Button) findViewById(R.id.btn4);
        but4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent newPage = new Intent(MainActivity.this, FCMActivity.class);
                startActivity(newPage);
            }
        });



    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    public void launchTicTacToe(View view) {
        Intent intent = new Intent(this, edu.neu.madcourse.numad17s_yudongwang.wordGame.MainActivity.class);
        startActivity(intent);
    }
}
