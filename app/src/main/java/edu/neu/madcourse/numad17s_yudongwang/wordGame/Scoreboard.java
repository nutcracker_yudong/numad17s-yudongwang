package edu.neu.madcourse.numad17s_yudongwang.wordGame;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import edu.neu.madcourse.numad17s_yudongwang.*;


public class Scoreboard extends Activity {


    private ListView listView;
    private List<String> content;
    SharedPreferences shared_preferences;
    SharedPreferences.Editor shared_preferences_editor;
    String records_string = "";
    private Button btn_sort_time;
    private Button btn_sort_score;
    private Button btn_sort_word;
    private Button btn_score_clear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scoreboard);

        btn_sort_time = (Button) this.findViewById(R.id.btn_sort_time);

        btn_sort_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortListByTime();
                updateList();
            }
        });

        btn_sort_score = (Button) this.findViewById(R.id.btn_sort_score);

        btn_sort_score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortListByScore();
                updateList();
            }
        });
        btn_sort_word = (Button) this.findViewById(R.id.btn_sort_word);

        btn_sort_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortListByWord();
                updateList();
            }
        });
        btn_score_clear = (Button) this.findViewById(R.id.btn_score_clear);

        btn_score_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shared_preferences_editor.putString("scoreboard", "");
                shared_preferences_editor.commit();
                records_string = "";
                content.clear();
                updateList();
            }
        });

        shared_preferences = PreferenceManager.getDefaultSharedPreferences(this);
        shared_preferences_editor = shared_preferences.edit();

        records_string = shared_preferences.getString("scoreboard", "Default");

        String[] records = records_string.split("\\$");
        content = new ArrayList<>();
        for (String s : records) {
            if (!s.isEmpty()) content.add(0, s);
        }

        updateList();
    }


    private void sortListByTime() {

        String[] records = records_string.split("\\$");
        content = new ArrayList<>();
        for (String s : records) {
            if (!s.isEmpty()) content.add(0, s);
        }
    }

    private void sortListByScore() {


        String[] records = records_string.split("\\$");
        content = new ArrayList<>();


        if (records.length > 1) {

            Arrays.sort(records, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return Integer.valueOf(o1.split(" ")[12]) - Integer.valueOf(o2.split(" ")[12]);
                }
            });
        }


        for (String s : records) {
            if (!s.isEmpty()) content.add(0, s);
        }
    }

    private void sortListByWord() {

        String[] records = records_string.split("\\$");
        content = new ArrayList<>();

        if (records.length > 1) {

            Arrays.sort(records, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o1.split(" ")[25].length() - o2.split(" ")[25].length();
                }
            });
        }

        for (String s : records) {
            if (!s.isEmpty()) content.add(0, s);
        }
    }

    public void updateList() {
        listView = (ListView) findViewById(R.id.list_scoreboard);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getApplicationContext(),
                R.layout.custom_listitem,
                R.id.list_item,
                content);
        //assign the adapter to listView
        listView.setAdapter(adapter);
    }
}
